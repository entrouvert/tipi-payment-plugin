from django.utils.translation import ugettext as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import TipiPluginModel

class TipiPlugin(CMSPluginBase):
    model = TipiPluginModel
    name = _(u'Formulaire de paiement TIPI')
    render_template = 'tipi_payment_plugin/tipi.html'
    text_enabled = True

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

plugin_pool.register_plugin(TipiPlugin)

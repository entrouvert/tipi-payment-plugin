#-*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext as _

from cms.models import CMSPlugin

SAISIES = (
    ('T', 'T'),
    ('M', 'M'),
    ('X', 'X'),
    )

class TipiPluginModel(CMSPlugin):
    url = models.URLField(help_text=_(u'addresse de paiement sur le site de la DGFiP'))
    numero_client = models.CharField(max_length=64, help_text=_(u'le numéro de régie attribué par la DGFiP'))
    saisie = models.CharField(max_length=1, choices=SAISIES)

